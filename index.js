const express = require('express');

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();
const PORT = 3000;
const PATH = require('path');

app.use(bodyParser.urlencoded({extended:false}));
app.use(cookieParser());
app.set('view engine', 'pug');



app.get('/',(req,res)=>{
    const name = req.cookies.username;
        res.render('index',{name });
});
app.get('/hello',(req,res)=>{
    const name = req.cookies.username;
    if (!name)
        res.render('hello');
    else
        res.redirect('/')
});
app.post('/hello',(req,res)=>{
    res.cookie("username",req.body.username);
    res.redirect('/')
});
app.post('/sign-out',(req,res)=>{
    res.clearCookie('username');
    res.redirect('/')
});

app.use((req,res,next)=>{
    const err = new Error('Not Found!');
    err.status=404;
    res.render('error',err);
    next(err);

});

app.listen(PORT);
